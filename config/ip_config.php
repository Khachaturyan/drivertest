<?php
return [
    'count' => env("IP_CHECK_COUNT", 20),
    'hour' => env("IP_CHECK_HOUR", 1)
];
