<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api\Auth')->group(function () {
    Route::post('token', 'ApiTokenController@store'); //create token
    Route::put('token', 'ApiTokenController@refresh'); //refresh token
    Route::delete('token', 'ApiTokenController@logout')->middleware('driver'); //delete token(logout)

    Route::post('send_sms', "SmsController@sendSms")->middleware('ip_check');// send sms and generate driver user

    Route::post('driver_lead', 'DriverLeadController@store');
});

Route::any('webhook', 'Api\BotController@index');

Route::middleware('driver')->namespace('Api')->group(function () {


    //route where has driver_id
    Route::middleware('driver_user')->group(function () {
        //driver payments
        Route::get('driver_payments/{driver_id}', 'DriverPaymentController@index');

        //driver payout
        Route::post('driver_payout/{driver_id}', 'DriverPayoutController@pay');

        //commission amount
        Route::post('commission/{driver_id}', 'CommissionController@index');
        //add driver ref
        Route::post('driver_ref/{driver_id}', 'DriverRefLeadController@driverRef');

    });

    Route::post('ref_leads', 'DriverRefLeadController@myLeads');


    // get DriverUser payment details
    Route::get('payment_details', 'DriverPaymentDetailsController@index'); // get driver requisite
    Route::post('payment_detail', 'DriverPaymentDetailsController@store'); //add requisite
    Route::post('check_bank_bic', 'DriverPaymentDetailsController@checkBic'); //add requisite
    Route::delete('payment_detail/{payment_detail_id}', 'DriverPaymentDetailsController@delete'); //remove requisite

    //driver payouts
    Route::get('driver_payouts', 'DriverPayoutController@index');

    //get drivers of driver user
    Route::get('driver_user/drivers', 'DriverUserDriversController@index');
    Route::post('driver_user/accept_offer', 'DriverUserDriversController@acceptOffer');

    Route::post('driver_user/apps', 'DriverUserDriversController@apps');

    // news
    Route::get('news', 'NewsController@index');
    Route::get('news/{news_id}', 'NewsController@show');

    Route::get('payout_statuses', 'DriverPayoutController@statuses');

});
