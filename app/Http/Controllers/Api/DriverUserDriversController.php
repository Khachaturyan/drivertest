<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Dunco\Models\DriverUserDevice;
use App\Http\Controllers\Controller;
use Dunco\Models\Driver;
use Dunco\Models\DriverOffer;
use Dunco\Models\DriverUser;
use Dunco\Services\Aggregator;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DriverUserDriversController extends Controller
{
    public function index(Request $request)
    {

        $driver_user = Auth::user();
        $brand = Auth::brand();

        $drivers = Driver::where('phone', $driver_user->phone)
            ->with(['driverGroup:id,customer_legal_entity_id,min_balance', 'driverGroup.customerLegalEntity:id', 'driverGroup.customerLegalEntity.brand:id,customer_legal_entity_id', 'park'])
            ->whereHas('park', function ($q) {
                $q->whereNull('deleted_at');
            })
            ->whereHas('driverGroup.customerLegalEntity.brand', function ($q) use ($brand) {
                if (!is_null($brand))
                    $q->where('id', $brand->id);
            })->get();

        $returnData = [];

        foreach ($drivers as $driver) {
            if (Carbon::now()->subMinutes(5) >= $driver->updated_at) {
                try {
                    $api = new Aggregator($driver->park);
                    $api->updateDriver($driver->api_id);
                    $startDate = now()->addDays(-1);
                    $endDate = now();
                    $api->saveDriverOrders($driver->api_id, $startDate, $endDate);
                    $api->saveDriverPayments($driver->api_id, $startDate, $endDate);
                    $driver->fresh();
                } catch (\Exception $e) {
                    Log::error($e->getMessage() . json_encode($e->getTrace()));
                }
            }
            $returnData[] = $driver->setAppends(['activeOffer', 'maxAvailableSum', 'conditionComment'])->toArray();
        }

        return $this->getSuccessResponse('ok', $returnData);
    }

    public function acceptOffer(Request $request)
    {
        $park_id = Driver::find($request->driver_id)->park_id;
        $driver = Auth::user()->driverOffers()->where('park_id', $park_id);
        $driver->detach();
        $driver->attach(DriverOffer::getCurrent($park_id));
        return $this->getSuccessResponse('ok');
    }

    public function apps(Request $request)
    {


        $driver_user = Auth::user();
        $dud = DriverUserDevice::updateOrCreate([
            'driver_user_id' => $driver_user->id,
            'device_id' => $request->device_id,
        ], [
            'device_name' => $request->device_name,
            'device_platform' => $request->device_platform,
            'apps' => $request->apps
        ]);

        return $this->getSuccessResponse('ok');
    }
}
