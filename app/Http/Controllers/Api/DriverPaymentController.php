<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dunco\Models\DriverPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DriverPaymentController extends Controller
{

    public function index(Request $request, int $driver_id)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'date_format:Y-m-d H:i:s|required_with:to',
            'to' => 'date_format:Y-m-d H:i:s|required_with:from',
            'skip' => 'required_with:take|integer',
            'take' => 'required_with:skip|integer'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $driver_payments = DriverPayment::where('driver_id', '=', $driver_id);

        if ($request->has(['from', 'to'])) {
            $data = $request->all();
            $driver_payments = $driver_payments->where('api_date', '>=', $data['from'])
                ->where('api_date', '<=', $data['to']);
        }

        $total = $driver_payments->count();

        if ($request->has(['skip', 'take'])) {
            $data = $request->all();
            $driver_payments->skip($data['skip'])->take($data['take']);
        }
        // сортировка по id - если api_date одинаковый для нескольких строк - могут быть задовения при отдаче
        $returnData = $driver_payments
            ->orderBy('api_date', 'desc')
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();

        return $this->getSuccessResponse(['message' => 'ok', 'total' => $total], $returnData);

    }
}
