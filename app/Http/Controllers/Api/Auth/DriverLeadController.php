<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Dunco\Models\DriversLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class DriverLeadController extends Controller
{
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->all();

        DriversLead::updateOrCreate([
            'phone'=>$data['phone']
        ], [ 
            'name'=>$data['name'],
        ]);


        $chat_id = env('TEL_CHAT_ID');
        $message = "*Новый водитель \n Имя-".$data['name']."\n Телефон-".$data['phone']."*";


        file_get_contents('https://api.telegram.org/bot' .
            env('TEL_TOKEN') .
            '/sendMessage?parse_mode=Markdown&text=' . urlencode($message).'&chat_id=' . $chat_id
        );

        return $this->getSuccessResponse('ok');
    }
}
