<?php

namespace App\Http\Controllers\Api\Auth;

use Dunco\Models\DriverUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use \Firebase\JWT\JWT;

class ApiTokenController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:drivers,phone|digits:11',
            'sms' => 'required|exists:sms_tokens,code|digits:4',
            'brand_id' => 'nullable|exists:brands,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }


        $data = $request->only(['phone', 'sms', 'brand_id']);
        $phone = $data['phone'];
        $sms = $data['sms'];
        $brand_id = $data['brand_id'];

        $driver = DriverUser::where('phone', '=', $phone)
            ->where('brand_id', '=', $brand_id)
            ->first();

        $smsTokens = $driver->smsToken();
        $smsTokens->where('send_at', '<', (now()->addMinutes(-10)))
            ->where('code', '!=', '8899')
            ->update(['expired' => true]);

        $smsTokens = $driver->smsToken;
        $check = $smsTokens->where('code', $sms);


        if ($check->count() > 0) {
            $check = $check->first();
            if ($check->used == true || $check->expired == true) {
                return $this->getFailResponse('sms_code_expired');
            } elseif ($check->used == false && $check->expired == false) {

                $api_token = Str::random(60);
                $refresh_token = Str::random(60);

                $key = env('JWT_KEY');
                $payload = array(
                    "user_id" => $driver->id,
                    "token" => $api_token,
                    "brand_id" => $brand_id
                );

                $refresh_payload = array(
                    "user_id" => $driver->id,
                    "token" => $refresh_token,
                    "brand_id" => $brand_id
                );

                $jwt_api_token = JWT::encode($payload, $key);
                $jwt_refresh_token = JWT::encode($refresh_payload, $key);


                $driver->api_token = Hash::make($api_token);
                $driver->refresh_token = Hash::make($refresh_token);

                $driver->api_token_created_at = now();
                $driver->save();

                if ($check->code !== '8899')
                    $check->used = true;

                $check->save();

                $returnData = [
                    'api_token' => $jwt_api_token,
                    'refresh_token' => $jwt_refresh_token
                ];

                return $this->getSuccessResponse('ok', $returnData);
            }
        } else {
            return $this->getFailResponse('wrong_sms_code');
        }
    }

    public function refresh(Request $request)
    {
        $key = env('JWT_KEY');
        $jwt = $request->header('RefreshToken');
        try {
            $payload = (array)JWT::decode($jwt, $key, array('HS256'));
        } catch (\Exception $e) {
            return $this->getFailResponse("wrong_api_token_or_expired");
        }
        $driver_user_id = $payload['user_id'];
        $token = $payload['token'];
        $brand_id = $payload['brand_id'];

        $driver_user = DriverUser::find($driver_user_id);
        $refresh_token = $driver_user->refresh_token;

        if (!Hash::check($token, $refresh_token)) {
            return $this->getFailResponse("invalid_refresh_token");
        }

        $api_token = Str::random(60);

        $new_payload = array(
            "user_id" => $driver_user->id,
            "token" => $api_token,
            "brand_id" => $brand_id
        );

        $jwt_api_token = Jwt::encode($new_payload, $key);
        $driver_user->api_token = Hash::make($api_token);
        $driver_user->api_token_created_at = now();
        $driver_user->save();

        $returnData = [
            'api_token' => $jwt_api_token
        ];

        return $this->getSuccessResponse('ok', $returnData);
    }

    public function logout(Request $request)
    {

        $jwt = $request->bearerToken();
        $key = env("JWT_KEY");
        $payload = (array)JWT::decode($jwt, $key, array('HS256'));
        $driver_user_id = $payload["user_id"];


        $driver_user = DriverUser::find($driver_user_id);
        $driver_user->api_token = null;
        $driver_user->refresh_token = null;
        $driver_user->api_token_created_at = null;
        $driver_user->barnd_id = null;
        $driver_user->fmc_id = null;
        $driver_user->save();

        return $this->getSuccessResponse("successfully_logout");
    }
}
