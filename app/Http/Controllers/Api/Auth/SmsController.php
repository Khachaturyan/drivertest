<?php

namespace App\Http\Controllers\Api\Auth;

use Dunco\Models\DriverUser;
use App\Http\Controllers\Controller;
use Dunco\Models\SmsToken;
use Dunco\Models\Driver;
use Dunco\NotificationChannels\Sms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class SmsController extends Controller
{
    public function sendSms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:drivers,phone|digits:11',
            'brand_id' => 'nullable|exists:brands,id',
            'fcm_id' => 'nullable'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->only(['phone', 'brand_id', 'fcm_id']);

        $phone = $data['phone'];
        $brand_id = $data['brand_id'];
        $fcm_id = $data['fcm_id'];

        $driver = Driver::where('phone', '=', $phone)->get();

        if ($driver->count() > 0) {
            $driver = $driver->first();
            $driver_user = DriverUser::updateOrCreate([
                'phone' => $driver->phone,
                'brand_id' => $brand_id
            ], [
                'fcm_id' => $fcm_id
            ]);
            $smsTokens = $driver_user->smsToken;
            foreach ($smsTokens as $smsToken) {
                if ($smsToken->send_at->diffInMinutes(now()) < 2) {
                    return $this->getFailResponse('cant_send_sms_more_than_2_minutes');
                } elseif ($smsToken->send_at->diffInMinutes(now()) > 2 && $smsToken->send_at->diffInMinutes(now()) < 10 && $smsToken->code !== '8899') {
                    $smsToken->expired = true;
                    $smsToken->save();
                }
            }

            if($phone === '79999999999'){
                return $this->getSuccessResponse("ok");
            }

            $code = SmsToken::generateCode();

            $sms = new Sms();
            $response = $sms->content($code)
                ->sender("DUNCO")
                ->phone($phone)
                ->clientId(1)
                ->send();
            if (array_key_exists('status', (array)$response)) {
                SmsToken::create([
                    'sms_tokenables_id' => $driver_user->id,
                    'sms_tokenables_type' => "Dunco\Models\DriverUser",
                    'code' => $code,
                    'send_at' => now()
                ]);
                return $this->getSuccessResponse("ok");
            } else {
                return $this->getFailResponse('sms_not_send');
            }
        }
        return $this->getFailResponse('driver_not_found');
    }
}
