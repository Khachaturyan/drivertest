<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\TelegramService;
use Dunco\Models\Driver;
use Dunco\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Dunco\Models\DriverRefLead;

class DriverRefLeadController extends Controller
{
    public function driverRef(int $driver_id, Request $request)
    {
        $driver = Driver::findOrFail($driver_id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone' => 'required|regex:/^7\d{10}$/ui'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $data = $request->all();
        $driverRefLead = DriverRefLead::create([
            'name' => $data['name'],
            'park_id' => $driver->park_id,
            'phone' => $data['phone'],
            'parent_driver_id' => $driver->id
        ]);

        $chat_id = $driverRefLead->park->getSetting(Setting::park_telegram_chat_id);
        $park_name = $driverRefLead->park->name;

        if (!empty($chat_id)) {
            $message = 'Водитель ' . $driver->fullName . ' пригласил ' . $driverRefLead->name . ' в парк ' . $park_name . "\n" . 'тел:' . $driverRefLead->phone;

            TelegramService::sendMessage($chat_id, $message);
        }

        return $this->getSuccessResponse('ref created');
    }

    public function myLeads()
    {
        $driver_user = Auth::user();

        $leads = DriverRefLead::whereHas('parentDriver', function ($q) use ($driver_user) {
            return $q->where('drivers.phone', $driver_user->phone);
        })
            ->orderByDesc('id')
            ->get();

        $return_data = [];
        $income = 0;

        foreach ($leads as $lead) {
            if ($lead->status === DriverRefLead::STATUS_COMPLETE) {
                $income += $lead->parkCondition->reward;
            }
            $return_data[] = [
                'id' => $lead->id,
                'phone' => $lead->phone,
                'name' => $lead->name,
                'status' => $lead->friendlyStatus,
                'order_count' => $lead->driver->orderCount ?? 0,
                'days_count' => now()->diffInDays($lead->driver->api_registration_date ?? now()),
                'order_count_target' => $lead->parkCondition->ride_count,
                'days_count_target' => $lead->parkCondition->interval,
            ];

        }

        return response()->json([
            'income' => $income,
            'data' => $return_data,
        ]);
    }
}
