<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dunco\Models\Driver;
use Dunco\Models\DriverOrder;
use Dunco\Models\Setting;
use Dunco\Services\Commission;
use Dunco\Services\DriverBalanceHoldService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommissionController extends Controller
{
    public function index(int $driver_id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'payment_detail_id' => 'required|integer|exists:payment_details,id',
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $driver_user = Auth::user();

        $driver = Driver::findOrFail($driver_id);
        $dir = $driver_user->paymentDetails()->findOrFail($request->payment_detail_id)->direction;
        $provider = $driver->driverGroup->paymentProvider($dir)->first();

        if (is_null($provider)) {
            return $this->getFailResponse("provider_fail", $validator->getMessageBag()->getMessages());
        }

        $include_commission = false;

        if ($request->has('include_commission')) {
            if ($request->include_commission == true)
                $include_commission = true;
        }

        $commission = (new Commission($request->amount, $provider, $include_commission))->review();

        $amount = $driver->maxAvailableSum;

        $returnData = [
            'commission' => $commission,
            'max_available_sum' => $amount,
            'min_available_sum' => $provider->min_sum
        ];

        return $this->getSuccessResponse('ok', $returnData);
    }


}
