<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\TelegramService;
use Illuminate\Http\Request;

class BotController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('my_chat_member')) {
            if (isset($request->my_chat_member['new_chat_member']) && isset($request->my_chat_member['new_chat_member']['user'])) {
                if ($request->my_chat_member['new_chat_member']['user']['id'] == '5085307686') {
                    $chat_id = $request->my_chat_member['chat']['id'];
                    TelegramService::sendMessage($chat_id, "Привет!\nЧтобы получать уведомления в эту группу, необходимо внести следующий ID:\n`" . $chat_id . "`\n в настроки парка вашего [кабинета](https://lk.dunco.ru/parks).");
                }
            }
        }
    }
}
