<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dunco\Models\Driver;
use Dunco\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'date_format:Y-m-d H:i:s|required_with:to',
            'to' => 'date_format:Y-m-d H:i:s|required_with:from',
            'skip' => 'required_with:take|integer',
            'take' => 'required_with:skip|integer'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }


        $driver_user = Auth::user();
        $brand = Auth::brand();

        $news = News::whereHas('park.drivers.driverGroup.customerLegalEntity.brand', function ($q) use ($brand) {
            if (!is_null($brand))
                $q->where('id', $brand->id);
        })->whereHas('park.drivers', function ($q) use ($driver_user) {
            $q->where('phone', $driver_user->phone);
        });


        if ($request->has(['from', 'to'])) {
            $data = $request->all();
            $news = $news->where('created_at', '>=', $data['from'])
                ->where('created_at', '<=', $data['to']);
        }

        $total = $news->count();

        if ($request->has(['skip', 'take'])) {
            $data = $request->all();
            $news->skip($data['skip'])->take($data['take']);
        }
        $returnData = $news->orderBy('created_at', 'desc')->orderBy('id', 'desc')->select(['id', 'park_id', 'short_description','description', 'created_at', 'title'])->get()->toArray();
        return $this->getSuccessResponse(['message' => 'ok', 'total' => $total], $returnData);
    }

    public function show($news_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'news_id' => 'required|integer|exists:news,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse('wrong_news_id');
        }

        $driver_user = Auth::user();
        $brand = Auth::brand();

        $news = News::where('id', $news_id)
            ->whereHas('park.drivers.driverGroup.customerLegalEntity.brand', function ($q) use ($brand) {
                if (!is_null($brand))
                    $q->where('id', $brand->id);
            })->whereHas('park.drivers', function ($q) use ($driver_user) {
                $q->where('phone', $driver_user->phone);
            })->get();


        return $this->getSuccessResponse('ok', $news->first()->toArray());


    }
}
