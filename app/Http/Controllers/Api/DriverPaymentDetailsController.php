<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Rules\DigitsEnum;
use Dunco\Models\BankBic;
use Dunco\Models\PaymentDetail;
use Dunco\Models\PaymentProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DriverPaymentDetailsController extends Controller
{


    public function index()
    {
        $payment_details = PaymentDetail::where('driver_user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
        $returnData = $payment_details->toArray();
        return $this->getSuccessResponse("ok", $returnData);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if(is_string ($request->all()['info'])){
            $rule = ['required', 'numeric', new DigitsEnum([11, 16, 20])];
            $info = $request->only(['info'])['info'];
        }else{
            $rule  = 'required|array';
            $info = $request->only(['info'])['info']['info'];
        }
        $rules = [
            'info' => $rule,
            'comment' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->only(['info', 'comment']);

        if (strlen($info) == 11) {
            $direction = PaymentProvider::WALLET;
        } elseif (strlen($info) == 16) {
            $direction = PaymentProvider::CARD;
        } elseif (strlen($info) == 20) {
            $direction = PaymentProvider::BANK;
        }

        $payment_detail = PaymentDetail::create([
            'infos' => is_string($data['info'])?(object)['info' => $data['info']]:$data['info'],
            'driver_user_id' => Auth::user()->id,
            'comment' => $data ['comment'],
            'direction' => $direction
        ]);

        $returnData = $payment_detail->toArray();

        return $this->getSuccessResponse('ok', $returnData);
    }

    public function delete($id){
        $paymentDetail = PaymentDetail::find($id);
        if(Auth::user()->id === $paymentDetail->driver_user_id){
            $paymentDetail->delete();
            return $this->getSuccessResponse('ok');
        }
        return $this->getFailResponse('error');

    }




    public function checkBic(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bic' => 'required|numeric|digits:9',
        ]);
        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $bank_bic = BankBic::where('bic', $request->only('bic')['bic'])->get();

        if ($bank_bic->count() > 0) {
            return $this->getSuccessResponse('true');
        } else {
            return $this->getSuccessResponse('false');
        }

    }

}
