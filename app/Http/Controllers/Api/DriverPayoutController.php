<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dunco\Jobs\PayoutJob;
use Dunco\Models\Driver;
use Dunco\Models\DriverPayout;
use Dunco\Models\PaymentDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class DriverPayoutController extends Controller
{

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'date_format:Y-m-d H:i:s|required_with:to',
            'to' => 'date_format:Y-m-d H:i:s|required_with:from',
            'skip' => 'required_with:take|integer',
            'take' => 'required_with:skip|integer'
        ]);


        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $driver_user_drivers = Auth::user()->drivers()->pluck('id');

        $driver_payouts = DriverPayout::whereIn('driver_id', $driver_user_drivers)->with('paymentDetail');

        if ($request->has(['from', 'to'])) {
            $data = $request->all();
            $driver_payouts = $driver_payouts->where('created_at', '>=', $data['from'])
                ->where('created_at', '<=', $data['to']);
        }

        if ($request->has(['skip', 'take'])) {
            $data = $request->all();
            $driver_payouts->skip($data['skip'])->take($data['take']);
        }
        $returnData = $driver_payouts->orderBy('id', 'desc')->get()->toArray();


        return $this->getSuccessResponse('ok', $returnData);

    }

    public function statuses()
    {
        $data = DriverPayout::STATUSES;
        $returnData = [];
        foreach ($data as $status => $label) {
            $returnData[] = [
                'status' => $status,
                'label' => $label
            ];
        }

        return $this->getSuccessResponse('ok', $returnData);
    }

    public function pay(Request $request, int $driver_id)
    {
        $validator = Validator::make($request->all(), [
            'sum' => 'required|numeric',
            'payment_detail_id' => 'required|integer|exists:payment_details,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }


        $data = $request->all();

        $payment_detail = PaymentDetail::findOrFail($data['payment_detail_id']);
        $driver = Driver::findOrFail($driver_id);

        $txn = microtime(true) * 10000 . rand(1000, 9999);

        PayoutJob::dispatch($driver, $payment_detail, $data['sum'], $txn)->onQueue('payouts');

        return $this->getSuccessResponse('ok');

    }
}
