<?php

namespace App\Http\Middleware;

use Dunco\Models\IPModel;
use App\Traits\ResponseTrait;
use Closure;

class IpCheck
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip_address = $request->ip();
        $phone = $request->phone;
        $count = IPModel::where('ip_address','=',$ip_address)
            ->where('created_at','>',now()->addHours(-config('ip_config.hour')))
            ->count();

        if($count < config('ip_config.count')){
            IPModel::create([
                'ip_address'=>$ip_address,
                'phone' => $phone
            ]);
            return $next($request);
        }else{
            return $this->getFailResponse('reach_max_request_count');
        }
    }
}
