<?php

namespace App\Http\Middleware;

use Dunco\Models\Driver;
use Dunco\Models\DriverUser;
use App\Traits\ResponseTrait;
use Closure;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ApiAuth
{
    use ResponseTrait;

    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('Authorization')) {
            return $this->getFailResponse('missing_authorisation_token');
        } else {
            $jwt = $request->bearerToken();
            $key = env("JWT_KEY");

            try {
                $payload = (array)JWT::decode($jwt, $key, array('HS256'));
            } catch (\Exception $e) {
                return $this->getFailResponse("wrong_api_token_or_expired");
            }

            $driver_user_id = $payload["user_id"];
            $token = $payload["token"];
            $brand_id = $payload["brand_id"];

            $check = Driver::where('phone', Auth::user()->phone)
                ->with(['driverGroup:id,customer_legal_entity_id', 'driverGroup.customerLegalEntity:id', 'driverGroup.customerLegalEntity.brand:id,customer_legal_entity_id'])
                ->whereHas('driverGroup.customerLegalEntity.brand', function ($q) use ($brand_id) {
                    if (!is_null($brand_id))
                        $q->where('id', $brand_id);
                })
                ->get()
                ->toArray();


            $driver_user = DriverUser::find($driver_user_id);

            $api_token = $driver_user->api_token;
            $api_token_created_at = $driver_user->api_token_created_at;

            $token_expired = false;
            if ($api_token_created_at->diffInMinutes(now()) > 30) {
                $token_expired = true;
            }

            if (Hash::check($token, $api_token) && $token_expired == false && !empty($check)) {
                return $next($request);
            } else {
                return $this->getFailResponse("wrong_api_token_or_expired");
            }
        }
    }
}
