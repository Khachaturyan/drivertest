<?php
namespace App\Extensions;

use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class DriverUserProvider implements UserProvider
{
    /**
     * The Mongo User Model
     */
    private $model;


    public function __construct(\Dunco\Models\DriverUser $userModel)
    {
        $this->model = $userModel;
    }

    public function retrieveByCredentials(array $credentials)    {}

    public function validateCredentials(Authenticatable $user, Array $credentials){}

    public function retrieveById($identifier) {}

    public function retrieveByToken($identifier, $token) {}

    public function updateRememberToken(Authenticatable $user, $token) {}
}
