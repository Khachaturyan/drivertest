<?php


namespace App\Services;

class TelegramService
{
    public static function sendMessage(string $chat_id, string $message, $parse_mode = 'Markdown', array $options = [])
    {
        $bot_token = '5085307686:AAEw2aEfoirAQ9d8j268NMcs0QA6iRIMQmk';

        $defaults = [
            'text' => $message,
            'chat_id' => $chat_id,
            'parse_mode' => $parse_mode,
        ];

        $options = json_encode(array_merge($options, $defaults));

        try {
            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://api.telegram.org/bot' . $bot_token . '/sendMessage',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $options,
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json'
                ],
            ]);

            curl_exec($curl);

            curl_close($curl);
        } catch (\Exception $e) {
            \Log::error('не смогли отправить telegram ' . $message . "\nОшибка:" . $e->getMessage());
        }
    }
}
