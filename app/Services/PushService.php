<?php

namespace App\Services;


use GuzzleHttp\Client;

class PushService
{
    public function sendPushNotification($fcm_token, $body, $id = null)
    {

        $url = "https://fcm.googleapis.com/fcm/send";

        $header = [
            'Authorization' => 'key=' . env("PROJECT_KEY"),
            'Content-Type' => 'application/json'
        ];


        $postdata = [
            "to" => $fcm_token,
            "notification" => [
                "body" => $body,
                "title" => "Dunco"
            ],
            "data" => [
                "body" =>  $body
            ]
        ];

        $client = new Client();

        return $client->request('post', $url,
            [
                'json' => $postdata,
                'headers' => $header
            ]);
    }
}
