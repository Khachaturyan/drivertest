<?php

namespace App\Services;

use Dunco\Models\Brand;
use Dunco\Models\DriverUser;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Request;

class DriverUserGuard implements Guard
{

    protected $provider;
    protected $user;
    protected $brand;

    public function __construct(UserProvider $provider)
    {
        $this->provider = $provider;
        $this->user = NULL;
        $this->brand = NULL;

    }

    public function user()
    {
        if (!is_null($this->user)) {
            return $this->user;
        }

        if (is_null(Request::bearerToken())) {
            return $this->user;
        }

        $jwt = Request::bearerToken();
        $key = env("JWT_KEY");

        $payload = (array)JWT::decode($jwt, $key, array('HS256'));

        $driver_user_id = $payload["user_id"];
        $this->user = DriverUser::find($driver_user_id);

        return $this->user;
    }

    public function brand()
    {
        if (!is_null($this->brand)) {
            return $this->brand;
        }

        if (is_null(Request::bearerToken())) {
            return $this->brand;
        }

        $jwt = Request::bearerToken();
        $key = env("JWT_KEY");

        $payload = (array)JWT::decode($jwt, $key, array('HS256'));
        $brand_id = $payload["brand_id"];
        $this->brand = Brand::find($brand_id);

        return $this->brand;
    }

    public function setBrand($brand_id)
    {
        $this->brand = Brand::find($brand_id);
        return $this;
    }

    public function checkDriver($driver_id)
    {
        $driver_user = $this->user;

        $drivers = $driver_user->drivers()
            ->where('id', $driver_id)
            ->get();
        if ($drivers->count() > 0) {
            return true;
        }
        return false;
    }

    public function id()
    {
        if ($user = $this->user()) {
            return $this->user()->getAuthIdentifier();
        }
    }

    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
        return $this;
    }

    public function guest()
    {
        return !$this->check();
    }

    public function check()
    {
        return !is_null($this->user());
    }

    public function validate(array $credentials = [])
    {
        return true;
    }
}
