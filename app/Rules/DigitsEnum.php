<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DigitsEnum implements Rule
{
    /**
     * @var array
     */
    public $enumArray;

    /**
     * DigitsEnum constructor.
     * @param array $enumArray\
     */
    public function __construct(array $enumArray)
    {
        $this->enumArray = $enumArray;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($this->enumArray as $enumVal){
            if($enumVal === strlen($value)){
                return true;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Digits count out of range';
    }
}
