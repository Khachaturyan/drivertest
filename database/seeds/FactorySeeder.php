<?php

use Illuminate\Database\Seeder;

class FactorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Dunco\Models\Driver::class, 100)->create();
        factory(\Dunco\Models\DriverUser::class, 100)->create();
        factory(\Dunco\Models\DriverPayment::class, 400)->create();
        factory(\Dunco\Models\PaymentDetail::class, 500)->create();
        factory(\Dunco\Models\DriverPayout::class, 2000)->create();
        factory(\Dunco\Models\DriverOrder::class, 50000)->create();
        factory(\Dunco\Models\News::class, 50)->create();
    }
}
