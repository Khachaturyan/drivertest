<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Dunco\Models\Driver;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Driver::class, function (Faker $faker) {
    $phone = $faker->numerify('79#########');
    $park_id = rand(1, \Dunco\Models\Park::all()->count());
    $park = \Dunco\Models\Park::findOrFail($park_id);

    return [
        'name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => '',
        'park_id' => $park_id,
        'api_id' => $faker->unique()->uuid,
        'phone' => $phone,
        'bankid' => $phone,
        'balance' => $faker->randomFloat(2, 1000, 10000),
        'driver_group_id' => $park->customerLegalEntity->driverGroups->random(),
        'api_registration_date' => $faker->dateTimeBetween('-30 days','+30 days'),
        'created_at' => $faker->dateTimeBetween('-30 days','+30 days'),
        'updated_at' => $faker->dateTimeBetween('-30 days','+30 days'),

    ];
});
