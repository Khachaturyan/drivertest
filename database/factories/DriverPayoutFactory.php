<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Dunco\Models\PaymentProvider;
use Faker\Generator as Faker;

$factory->define(\Dunco\Models\DriverPayout::class, function (Faker $faker) {
    $driver = \Dunco\Models\Driver::whereHas('driverUser.paymentDetails')->get()->random();
    $driverGroup = $driver->driverGroup;
    $driverUser = $driver->driverUser;

    $paymentDetail = $driverUser->paymentDetails->random();

    $provider = null;
    do {
        $provider = $driverGroup->paymentProvider(array_rand(Dunco\Models\PaymentProvider::DIRECTIONS))->first();
    } while (is_null($provider));


    $amount = $faker->randomFloat(2, 100, 9500);
    $commService = new \Dunco\Services\Commission($amount, $provider);

    return [
        'driver_id' => $driver->id,
        'payment_provider_id' => $provider->id,
        'payment_detail_id' => $paymentDetail->id,
        'amount' => $amount,
        'commission' => $commService->review(),
        'status' => array_rand(\Dunco\Models\DriverPayout::STATUSES),
        'txn_id' => microtime(true) * 10000 . rand(1000, 9999),
        'created_at' => $faker->dateTimeBetween('-30 days','+30 days'),
        'updated_at' => $faker->dateTimeBetween('-30 days','+30 days'),
    ];
});
