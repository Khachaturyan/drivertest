<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\Dunco\Models\DriverOrder::class, function (Faker $faker) {

    $driver = \Dunco\Models\Driver::all()->random();
    $api_id = $faker->unique()->uuid;
    $api_date = $faker->dateTimeBetween('-30 days', '+30 days');
    $amount = $faker->randomFloat(2, 100, 1028);
    $status = [
        'Нет машины',
        'waiting',
        'none',
        'failed',
        'driving',
        'cancelled',
        'transporting',
        'complete',
        'Назначен',
        'Отменен',
        'Завершен',
    ];

    return [
        'driver_id' => $driver->id,
        'api_id' => $api_id,
        'api_date' => $api_date,
        'amount' => $amount,
        'status' => $status[array_rand($status)]
    ];
});
