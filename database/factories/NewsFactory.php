<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Dunco\Models\News;
use Faker\Generator as Faker;


$factory->define(News::class, function (Faker $faker) {
    return [
        'park_id' => \Dunco\Models\Park::all()->random()->id,
        'title' => $faker->realText(30),
        'short_description' => $faker->realText(100),
        'description' => $faker->randomHtml(),
        'created_at' => $faker->dateTimeBetween('-30 days'),
    ];
});
