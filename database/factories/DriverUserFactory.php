<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Dunco\Models\Driver;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Dunco\Models\DriverUser::class, function (Faker $faker) {
    $phone = Driver::all()->random()->phone;

    return [
        'phone' => $phone,
        'api_token' => $faker->unique()->uuid,
        'refresh_token' => $faker->unique()->uuid,
        'created_at' => now(),
        'updated_at' => now(),

    ];
});
