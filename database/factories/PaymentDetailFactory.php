<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Dunco\Models\PaymentDetail;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(PaymentDetail::class, function (Faker $faker) {
    $du = Dunco\Models\DriverUser::all()->random();

    $rand_info = [
        $faker->numerify('79#########'),
        $faker->numerify('################')
    ];

    $rand = rand(0, 1);
    $info = $rand_info[$rand];

    if ($rand == 0)
        $direction = \Dunco\Models\PaymentProvider::WALLET;
    if ($rand == 1)
        $direction = \Dunco\Models\PaymentProvider::CARD;

    return [

        'driver_user_id' => $du->id,
        'comment' => $faker->realText(100),
        'info' => $info,
        'direction' => $direction

    ];
});
