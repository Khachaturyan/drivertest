<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DemoData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $cle = \Dunco\Models\CustomerLegalEntity::create(
            [
                'legal_name'=>'CLE legal name',
                'legal_form'=>'ООО',
                'tin'=>'11111',
                'authorized_document'=>'-',
                'legal_address'=>'-',
                'psrn'=>'-',
                'position'=>'-',
                'position_genitive'=>'-',
                'ceo_name'=>'-',
                'ceo_name_genitive'=>'-',
                'email'=>'-',
                'kpp'=>'-'
            ]
        );

        $park = \Dunco\Models\Park::create([
            'name'=>'Test Park',
            'aggregator'=>'CityMobil',
            'customer_legal_entity_id'=>$cle->id,
            'support_phone'=>'75555555555'
        ]);

        $payment_provider = \Dunco\Models\PaymentProvider::create([
            'type'=>'Qiwi',
            'name'=>'Qiwi Wallet',
            'customer_legal_entity_id'=>$cle->id,
            'direction_type'=>'wallet',
            'min_sum'=>100
        ]);
        $driver_group = \Dunco\Models\DriverGroup::create([
            'name'=>'ALl Drivers',
            'customer_legal_entity_id'=>$cle->id,
            'provider_id_qiwi'=>$payment_provider->id,
        ]);

        $driver = \Dunco\Models\Driver::create([
            'park_id'=>$park->id,
            'api_id'=>'6b8a0e28f320b3fbe6d8f605192acf16',
            'phone'=>'79999999999',
            'name'=>'Test',
            'last_name'=>'Test',
            'middle_name'=>'Test',
            'bankid'=>'79999999999',
            'blocked'=>false,
            'balance'=>10000,
            'driver_group_id'=>$driver_group->id,
            'api_registration_date'=>'2020-10-05 09:06:41',
        ]);

        $driver_user = \Dunco\Models\DriverUser::create([
            'phone'=>'79999999999'
        ]);

        \Dunco\Models\SmsToken::create([
            'code'=>'8899',
            'sms_tokenables_type'=>'Dunco\Models\DriverUser',
            'sms_tokenables_id'=>$driver_user->id,
            'used'=>false,
            'expired'=>false
        ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
